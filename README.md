# COMP 348 Assignment 1
Please find the source code in `src/`
## Setup instructions
To setup the mathpipe command, run the `setup.sh` file

```bash
source ./setup.sh
```

To compile the code use the `make` command

```bash
make mathpipe
```
If you do not have the `make` command, use the following command:

```bash
gcc -Wall -std=c99 src/*.c -o mathpipe
```

