#include <stdbool.h>
#include <stdlib.h>
#include "../src/singular.h"
#define assertEqual(a,b) if ((a) != (b)) {printf("values not equal in %s at line %d\n", __FILE__, __LINE__);}
#define TEST_COUNT 9

typedef void (* test_ptr)();

// test print
static void test1() {
    
    double arr[] = {1.45, 2.51, 4.3654, 6.7, 9};
    printf("is_prec_init: %s\n", is_prec_init()? "true": "false");
    set_prec(1);
    print(arr, 3);
}
static void test2() {
    double arr[] = {2.4, 5, 6.7, 9};
    shift(arr, 3, 3.5);
    assertEqual(arr[0], 5.9);
    assertEqual(arr[1], 8.5);
    assertEqual(arr[2], 10.2);
    assertEqual(arr[3], 9);
}
// filter test
static void test3() {
    double arr[] = {2.4, 5, 6.7, 9};
    size_t size = filter(arr, 3, EQ, 10);
    assertEqual(size, 0);
    assertEqual(arr[0], 6.7);
    assertEqual(arr[1], 6.7);
    assertEqual(arr[2], 6.7);
    assertEqual(arr[3], 9);

}

static void test4() {
    double arr[] = {2.4, 5, 10, 9, 6.7, 11};
    size_t size = filter(arr, 5, NEQ, 10);
    assertEqual(size, 4);
    assertEqual(arr[0], 2.4);
    assertEqual(arr[1], 5);
    assertEqual(arr[2], 9);
    assertEqual(arr[3], 6.7);
    assertEqual(arr[4], 6.7);
    assertEqual(arr[5], 11);
}

static void test5() {
    double arr[] = {2.4, 5, 10, 9, 6.7, 11};
    size_t size = filter(arr, 5, NEQ, 10);
    assertEqual(size, 4);
    assertEqual(arr[0], 2.4);
    assertEqual(arr[1], 5);
    assertEqual(arr[2], 9);
    assertEqual(arr[3], 6.7);
    assertEqual(arr[4], 6.7);
    assertEqual(arr[5], 11);
}

static void test6() {
    double arr[] = {2.4, 12, 13, 9, 6.7, 11, 10};
    size_t size = filter(arr, 7, GEQ, 10);
    assertEqual(size, 4);
    assertEqual(arr[0], 12);
    assertEqual(arr[1], 13);
    assertEqual(arr[2], 11);
    assertEqual(arr[3], 10);
    assertEqual(arr[4], 10);
    assertEqual(arr[5], 10);
    assertEqual(arr[6], 10);
}

static void test7() {
    double arr[] = {2.4, 12, 13, 9, 6.7, 10, 11};
    size_t size = filter(arr, 7, LEQ, 10);
    assertEqual(size, 4);
    assertEqual(arr[0], 2.4);
    assertEqual(arr[1], 9);
    assertEqual(arr[2], 6.7);
    assertEqual(arr[3], 10);
    assertEqual(arr[4], 11);
    assertEqual(arr[5], 11);
    assertEqual(arr[6], 11);
}

static void test8() {
    double arr[] = {2.4, 12, 13, 9, 6.7, 10, 11};
    size_t size = filter(arr, 7, LESS, 10);
    assertEqual(size, 3);
    assertEqual(arr[0], 2.4);
    assertEqual(arr[1], 9);
    assertEqual(arr[2], 6.7);
    assertEqual(arr[3], 11);
    assertEqual(arr[4], 11);
    assertEqual(arr[5], 11);
    assertEqual(arr[6], 11);
}

static void test9() {
    double arr[] = {2.4, 12, 13, 9, 6.7, 10, 11};
    size_t size = filter(arr, 7, GREATER, 10);
    assertEqual(size, 3);
    assertEqual(arr[0], 12);
    assertEqual(arr[1], 13);
    assertEqual(arr[2], 11);
    assertEqual(arr[3], 11);
    assertEqual(arr[4], 11);
    assertEqual(arr[5], 11);
    assertEqual(arr[6], 11);
}


test_ptr my_tests[TEST_COUNT] = {
 &test1,
 &test2,
 &test3,
 &test4,
 &test5,
 &test6,
 &test7,
 &test8,
 &test9
};

void test_singular() {
    // parse inputs
    for (int i = 0; i < TEST_COUNT; i++) {
        printf("run test %d\n", i+1);
        (* my_tests[i])();
    }
}