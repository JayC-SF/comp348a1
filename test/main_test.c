#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include "../src/aggregate.h"
#include "../src/singular.h"
#include "test_aggregate.h"
#include "test_singular.h"


int main(int argc, char * argv[]) {
    // parse inputs
    printf("running tests for aggregate.c\n\n");
    test_aggregate();
    printf("running tests for singular.c\n");
    test_singular();
    return 0;
}
