#include <stdio.h>
#include <string.h>
#include "../src/aggregate.h"
#include <stdbool.h>

#define assertEqual(a,b) if ((a) != (b)) {printf("values not equal in %s at line %d\n", __FILE__, __LINE__);}
#define TEST_COUNT 7

typedef void (* test_ptr) ();
// test if wrong function name does not work.
static void test1_throws() {
    double arr[] = {1,2,3,4};
    aggregate("AVGA", arr, 4);
}
// test if non positive size aborts program.
static void test2_throws() {
    double arr[] = {1,2,3,4};
    aggregate("AVG", arr, 0);
}

// test if non positive size aborts program.
static void test3_throws() {
    double arr[] = {1,2,3,4};
    aggregate("AVG", arr, -1);
}

// test if non positive size aborts program.
static void test4() {
    double arr[] = {1,2,3,4};
    double result = aggregate("AVG", arr, 4);
    assertEqual(result, 2.5);
}
static void test5() {
    double arr[] = {-1,2,3.5,4.8, 1, 1, 1, 1, 4, 6, 7, 3};
    double result = aggregate("COUNT", arr, 12);
    assertEqual(result, 12  );
}

static void test6() {
    double arr[] = {-1,2,3.5,4.8, 1, 1, 1, 1,-2 ,4, 6, 7, 3, -5};
    double result = aggregate("MIN", arr, 12);
    // only take in consideration the first 12 elements.
    assertEqual(result, -2 );
}

static void test7() {
    double arr[] = {-1,2,3.5,4.8, 1, 1, 1, 1,-2 ,4, 6, 7, 3, -5};
    double result = aggregate("MAX", arr, 12);
    // only take in consideration the first 12 elements.
    assertEqual(result, 7 );
}

static void test8() {
    double arr[] = {-1,2,3.5,4.8};
    double result = aggregate("SUM", arr, 4);
    assertEqual(result, 9.3);
}

static void test9() {
    double arr[] = {-1,2,3.5,4.8};
    double result  = aggregate("AVG", arr, 4);
    assertEqual(result, 2.325);
}

static void test10() {
    double arr[] = {-1,2,3.5,4.8, 1, 1, 1, 1,-2 ,4, 6, 7, 3, -5};
    double result = aggregate("PAVG", arr, 14);
    assertEqual(result, 1);
}

static void test11_throws() {
    aggregate("MIN", NULL, 1);
}
static void test12_throws() {
    double arr[] = {-1,2,3.5,4.8, 1, 1, 1, 1,-2 ,4, 6, 7, 3, -5};
    aggregate("miN", arr, 0);
}

static void test13_throws() {
    aggregate("Max", NULL, 1);
}
static void test14_throws() {
    double arr[] = {-1,2,3.5,4.8, 1, 1, 1, 1,-2 ,4, 6, 7, 3, -5};
    aggregate("MaX", arr, 0);
}

static void test15_throws() {
    double arr[] = {-1,2,3.5,4.8, 1, 1, 1, 1,-2 ,4, 6, 7, 3, -5};
    aggregate("MaX", arr, 0);
}

static test_ptr my_tests[TEST_COUNT] = {
    // &test1_throws, 
    // &test2_throws, 
    // &test3_throws, 
    &test4, 
    &test5, 
    &test6,
    &test7, 
    &test8, 
    &test9, 
    &test10
};

void test_aggregate() {
    // parse inputs
    for (int i = 0; i < TEST_COUNT; i++) {
        printf("run test %d\n", i+1);
        (* my_tests[i])();
    }
}
