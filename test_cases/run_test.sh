#!/bin/bash
TEST_NUM=1

function check() {
	diff ./outputs/out$TEST_NUM.txt ./solutions/solution$TEST_NUM.txt
	if [[ $? -eq 0 ]]
	then
	        echo "Test $TEST_NUM passed!"
	else
        	echo "Test $TEST_NUM failed."
			exit(1)
	fi

	((TEST_NUM++))
}
# format: cat input$TEST_NUM.txt | `insert your mathpipe command here` > out$TEST_NUM.txt
# test 1
cat ./inputs/input$TEST_NUM.txt | ./mathpipe sUM -SizE=4 | ./mathpipe SHIFT 5.5 &> ./outputs/out$TEST_NUM.txt
check

# test 2
# cat input$TEST_NUM.txt <put your command here>  &> out$TEST_NUM.txt
# check

