mathlib: src/*.c
	gcc -Wall -std=c99 src/mathpipe.c src/aggregate.c src/singular.c -o mathpipe

test: src/aggregate.c src/singular.c test/*.c
	gcc -Wall src/aggregate.c src/singular.c test/*.c -o mathpipe

clean:
	rm mathpipe