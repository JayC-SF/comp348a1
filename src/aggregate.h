
// Define guard
#ifndef AGGREGATE_H
// define AGGRETATE_H so that this section is defined once on imports
#define AGGREGATE_H
// Prototype for aggregate.c
double aggregate(const char* func, double* arr, int size);
#endif