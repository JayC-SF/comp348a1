#ifndef SINGULAR_H
// might have to include the stdio.h as well?
#define SINGULAR_H

#include <stdio.h>
#include <stdbool.h>
// Create Filter enum
#define FILTER_NUM 6

typedef enum filter_type {EQ = 0, NEQ = 1, GEQ = 2, LEQ = 3, LESS = 4, GREATER = 5} filter_type;
extern const char * const str_filter_type[FILTER_NUM];


// print function prototype
void print(double a[], size_t size);
// print shift prototype
void shift(double a[], size_t size, double by);
// filter function prototype
size_t filter(double a[], size_t count, filter_type t, double threshold);

// precision getters and setters
typedef struct {
    int val;
    bool is_init;
} optional_int;

void set_prec(int p);
bool is_prec_init();
int get_prec();
char * get_prec_format();

#endif