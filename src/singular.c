#include "singular.h"
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
const char * const str_filter_type[FILTER_NUM] = {"EQ", "NEQ", "GEQ", "LEQ", "LESS", "GREATER"};


// optional int where we don't know if the optional int is defined.
static optional_int precision = {0, false};
void set_prec(int p) {
    if (p < 0) {
        fprintf(stderr, "FATAL ERROR in %s at line %d: precision is negative `%d`.\n", __FILE__, __LINE__, p);
        exit(EXIT_FAILURE);
    }
    precision.val = p;
    precision.is_init = true;
}

bool is_prec_init() {
    return precision.is_init;
}

int get_prec() {
    if (!precision.is_init) {
        fprintf(stderr, "FATAL ERROR in %s at line %d: attempt to return undefined precision.\n", __FILE__, __LINE__);
        exit(EXIT_FAILURE);
    }
    return precision.val;
}

int num_of_char(int n) {
    int count = n < 0? 1:0;
    while(n != 0) {
        n = n/10;
        count++;
    }
    return count;
}

// this function allocates memory, it returns the precision format of a string to print with.
// for example if prec is 6 then it will return "%.6lf"
char * get_prec_format() {
    // format : "%.<number>lf "
    // we have 5 extra character bytes needed for the string
    // add 5 to count the number of total characters `%.lf\0`
    char * format;
    // if we have the precision already initialized, we create the precision format string.
    if (is_prec_init()) {
        int len = num_of_char(get_prec()) + 5;
        format = (char *) calloc(len,  sizeof(char));
        strcpy(format, "%.");
        sprintf(format + 2, "%d", get_prec());
        strcat(format, "lf");
    }
    else {
        // since precision is not initialized, we return the default string instead.
        char * tmp_format = "%lf";
        format = (char *) calloc(strlen(tmp_format) + 1,  sizeof(char));
        strcpy(format, tmp_format);
    }

    return format; // returns a string like so "%.<number>lf" where <number> is the precision number.
}

// 
static bool _eq(double a, double b) {
    return (a == b);
}
// 
static bool _neq(double a, double b) {
    return a != b;
}
// 
static bool _geq(double a, double b) {
    return a >= b;
}
// 
static bool _leq(double a, double b) {
    return a <= b;
}
// 
static bool _less(double a, double b) {
    return a < b;
}
// 
static bool _greater(double a, double b){
    return a > b;
}

// 
typedef bool (* operands_ptr) (double, double);

// 
static operands_ptr f_operands[] = {&_eq, &_neq, &_geq, &_leq, &_less, &_greater};

// This function prints the content of an array.
// This function assumes the checks are done before. No checks on the array being null
// nor the size of being 0 are done here. It is expected from the user to perform those.
// 
void print(double a[], size_t size) {
    // use stack memory instead of heap.
    // here we have a format for `%%%sf`, we first escape the first % and leave %s for the precision.
    // store current length for memory allocation in the future. Add 1 for count of \0
    char * format = get_prec_format();
    // since we know that there are less characters, we use the same memory location.

    for(int i = 0; i<size; i++) {
        // Any elements before the last one are printed with a space.
        // print with out a space because \n is added instead.
        printf(format, a[i]);
        // print "\n" if i is the last index, otherwise print a space.
        printf("%c", i == size-1 ? '\n': ' ');
    }
    // once we are done printing, we free the format as it is no longer useful.
    free(format);
}
// print shift prototype
void shift(double a[], size_t size, double by) {
    for (int i = 0; i<size; i++)
        a[i] += by;
}
// filter function prototype
size_t filter(double a[], size_t count, enum filter_type t, double threshold) {
    // Loop throuhg the array, and check for the condition.
    // If the condition is not met, we remove the element
    // and the elements after are shifted by one cell to the front.
    for (int i = 0; i < count; i++) {
        // Use the enum value to get the correct index in the function pointer array
        // Use the function pointer of the appropriate index to call the right boolean condition
        // We use the condition on a[i] and the threhshold
        bool result = (* f_operands[t]) (a[i], threshold);
        if (!result) {
            // decrement now, 
            // and loop to shift the whole array removing a single element
            count--;
            for (int j = i; j < count; j++) {
                a[j] = a[j+1];
            }
            // we decrement i because the next value is taking the current place
            // of i. Therefore, we need to make sure we do not skip an element.
            i--;
        }
    }
    return count;
}