#include <string.h>
#include <strings.h>
#include <stdio.h>
#include <stdlib.h>
// Check for positive value.
#define IS_POSITIVE(a) ((a) > 0)

// Number of aggregate functions implemented and ready to use.
#define AGGREGATE_FN_SIZE 6

// Defines a function pointer as a `func_ptr`
typedef double (* func_ptr)(double *, int);

// this function does the necessary check 
static void throw_if_empty_array(double * arr, int size) {
    // If array is null, return error
    if (arr == NULL) {
        // print error message in stderr.
        fprintf(stderr, "FATAL ERROR in %s at line %d: array is NULL.\n", __FILE__, __LINE__);
        exit(EXIT_FAILURE);
    }
    // Check if the size is not positive using Macro above.
    if (!IS_POSITIVE(size)) {
        fprintf(stderr, "FATAL ERROR in %s at line %d: `size` is not positive.\n", __FILE__, __LINE__);
        exit(EXIT_FAILURE);
    }
}
static double _count(double* arr, int size) {
    return size;
}

static double _min(double* arr, int size) {
    throw_if_empty_array(arr, size);
    // since we know the size is not 0 we can take the first number.
    double  min = arr[0];
    for(int i = 1; i < size; i++) {
        // current value is smaller than min stored, we update min for current value.
        if (arr[i] < min) {
            min = arr[i];
        }
    }
    return min;
}

static double _max(double* arr, int size) {
    throw_if_empty_array(arr, size);
    // since we know the size is not 0 we can take the first number.
    double  max = arr[0];
    for(int i = 1; i < size; i++) {
        // current value is greater than max stored, we update max for current value.
        if (max < arr[i]) {
            max = arr[i];
        }
    }
    return max;
}

static double _sum(double* arr, int size) {
    double sum = 0;
    for(int i = 0; i < size; i++) {
        // sum numbers of the array.
        sum += arr[i];
    }
    return sum;
}

static double _avg(double* arr, int size) {
    throw_if_empty_array(arr, size);
    return _sum(arr, size)/size;
    
}

static double _pavg(double* arr, int size) {
    // (min + max)/2
    return (_min(arr, size) + _max(arr, size))/2;
}

// Create function pointer array.
static func_ptr f_array[AGGREGATE_FN_SIZE] = {&_count, &_min, &_max, &_sum, &_avg, &_pavg};

// Create parallel string array for the names of the functions.
static char * funcnames[AGGREGATE_FN_SIZE] = {"COUNT", "MIN", "MAX", "SUM", "AVG", "PAVG"};

// double result = aggregate("MIN", arr, 5)
double aggregate(const char * func, double * arr, int size) {
    // Loop through the function list to find the appropriate function.
    // Once the function is found, compute the function on the array.
    for (int i = 0; i < AGGREGATE_FN_SIZE; i++) {
        // compare strings
        if (strcasecmp(func, funcnames[i]) == 0) {
         // store the results of the return value into the memory allocation of result.
            return (* f_array[i])(arr, size);
        }
    }

    // Function has not been found, return an error.
    fprintf(stderr, "FATAL ERROR in %s at line %d: func \"%s\" not supported.\n", __FILE__, __LINE__, func);
    exit(EXIT_FAILURE);
    
}

