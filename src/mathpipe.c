#include <stdio.h>
#include <string.h>
#include <strings.h>
#include <stdlib.h>
#include <stdbool.h>
#include <ctype.h>
#include "singular.h"
#include "aggregate.h"

#define OPERATION_NUM 9
#define INITIAL_NUM_EL 30
#define MAX_ROW_NUM 256

typedef enum operation {
    // aggregate
    COUNT  = 0, 
    MIN    = 1,
    MAX    = 2,
    SUM    = 3,
    AVG    = 4,
    PAVG   = 5,
    // singular
    PRINT  = 6,
    FILTER = 7,
    SHIFT  = OPERATION_NUM - 1
} operation;

// structure to store input_buffer;
typedef struct {
    // array pointer location
    void * arr;
    // current length of the array
    int len;
    // current capacity of the array.
    int capacity;
    // defines the size in bits of each element. i.e char would be a byte, int -> 4 bytes.
    size_t el_size;
} arraylist;

static const char * const str_operations[OPERATION_NUM] = {
    "COUNT",
    "MIN",
    "MAX",
    "SUM",
    "AVG",
    "PAVG",
    "PRINT",
    "FILTER",
    "SHIFT"
};


char * strcistr(const char * haystack, const char * needle) {
    // // get the length of both strings.
    // malloc space for the strings.
    // copy the strings in the 
    for(; *haystack != '\0'; haystack++) {
        const char * tmp_needle = needle;
        const char * tmp_hay = haystack;
        while(tolower(*tmp_needle) == tolower(*tmp_hay) && *tmp_needle != '\0'&& *tmp_hay != '\0') {
            tmp_needle++;
            tmp_hay++;
        }
        // found the needle in the haystack.
        if (*tmp_needle == '\0')
            return (char *) haystack;
        // we reached the end of haystack and needle still had characters left.
        else if (*tmp_hay == '\0')
            return NULL;
    }
    return NULL;
}

// PARAMS
static optional_int size_param = {0, false};
static optional_int operation_param = {0, false};
static double threshold_param;
static filter_type f_type_param;

int get_int(optional_int * oi) {
    if(oi == NULL) {
        fprintf(stderr, "FATAL ERROR in %s at line %d: optional_int is NULL.\n", __FILE__, __LINE__);
        exit(EXIT_FAILURE);
    }
    if (oi->is_init)
        return oi->val;
    fprintf(stderr, "FATAL ERROR in %s at line %d: attempt to retrieve optional int, but it is not initialized.\n", __FILE__, __LINE__);
    exit(EXIT_FAILURE);
}

void set_int(optional_int * oi, int i) {
    if(oi == NULL) {
        fprintf(stderr, "FATAL ERROR in %s at line %d: optional_int is NULL.\n", __FILE__, __LINE__);
        exit(EXIT_FAILURE);
    }
    oi->val = i;
    oi->is_init = true;
}

// create_default_array, uses malloc
arraylist * create_default_array(size_t el_size) {
    arraylist * r = malloc(sizeof(arraylist));
    r->arr = NULL;
    r->len = 0;
    r->capacity = 0;
    r->el_size = el_size;
    return r;
}

// This function serves as allocating the memory of the array in the arraylist.
static void * allocate_mem(arraylist * a, int capacity) {
    if (a == NULL) {
        fprintf(stderr, "FATAL ERROR in %s at line %d: arraylist is NULL.\n", __FILE__, __LINE__);
        exit(EXIT_FAILURE);
    }
    // check if the new capacity is not smaller than the length.
    if (capacity < a->len){
        fprintf(stderr, "FATAL ERROR in %s at line %d: arraylist is NULL.\n", __FILE__, __LINE__);
        exit(EXIT_FAILURE);
    }
    // set the new capacity.
    a->capacity = capacity;
    // realloc behaves like malloc if array is originally NULL.
    a->arr = realloc(a->arr, capacity * a->el_size);
    // check if realloc is not able to allocate memory.
    if (a->arr == NULL) {
        fprintf(stderr, "FATAL ERROR in %s at line %d: array is NULL.\n", __FILE__, __LINE__);
        exit(EXIT_FAILURE);
    }
    return a->arr;
}

// Get pointer at index returns the pointer of the memory location given an index.
static void * get_ptr_at_index(arraylist * a, int index) {
    if (a == NULL) {
        fprintf(stderr, "FATAL ERROR in %s at line %d: arraylist is NULL.\n", __FILE__, __LINE__);
        exit(EXIT_FAILURE);
    }
    // if the index is out of bounds, abort.
    if (index >= a->len || index < 0) {
        fprintf(stderr, "FATAL ERROR in %s at line %d: index out of bounds.\n", __FILE__, __LINE__);
        exit(EXIT_FAILURE);
    }
    // this is the equivalent of doing arr[len], but since we are dealing with void pointers
    // we have to manually calculate the index.
    // example:
    // if we have an int array for this arraylist, then:
    // we can access the index 3 by performing this:
    // arr + (sizeof(int) * 3); // knowing that arr is a void * pointer.
    return a->arr + (a->el_size * index);
}

// this function retrieves an element from the arraylist
// as mentioned earlier, it uses the same index formula.
void get_element(arraylist * a, int index, void * dest_ptr) {
    void * index_ptr = get_ptr_at_index(a, index);
    memcpy(dest_ptr, index_ptr, a->el_size);
}

// This function sets the value of an element at a specific index.
void set_element(arraylist * a, int index, void * src_ptr) {
    // get pointer to point at the right address for setting the value.
    void * index_ptr = get_ptr_at_index(a, index);
    memcpy(index_ptr, src_ptr, a->el_size);
}

// this function adds a new value to the arraylist.
// it uses a generic pointer that points to the location of the new value
// it then copies the value by using the el_size and memcpy.
// src_ptr is the address at which we return the value.
void add_element(arraylist * a,  void * src_ptr) {
    if (a == NULL) {
        fprintf(stderr, "FATAL ERROR in %s at line %d: arraylist is NULL.\n", __FILE__, __LINE__);
        exit(EXIT_FAILURE);
    }
    // check if it's empty.
    if (a->arr == NULL)
        a->arr = allocate_mem(a, INITIAL_NUM_EL);
    // reallocate if necessary.
    // double the capacity of the array.
    // call function allocate_mem
    if (a->len >= a->capacity)
        a->arr = allocate_mem(a, a->capacity * 2);

    // increment the length after successfully added the element.    
    a->len++;
    set_element(a, a->len - 1, src_ptr);
}

// This function squishes an arraylist, by reallocating the memory to the right size for all elements.
// it essentially sets the memory capacity to the exact number of elements in the array.
void squish_elements(arraylist * a) {
    if (a->len < a->capacity) {
        a->arr = allocate_mem(a, a->len);
    }
}

void free_arraylist(arraylist * a) {
    // check if a is not null 
    if (a != NULL) {
        free(a->arr);
        free(a);
    }
}
// Parses parameters in the form of "param=value".
// this function allocated memory.
bool is_integer(char * s) {
    char * t = s;
    int i;
    for (i = 0; t[i] != '\0'; i++) {
        if (i == 0 && t[i] == '-')
            continue;
        // if t[i] is outside of 0 - 9 range character then return false
        if  (!isdigit(t[i])) {
            return false;
        }
    }
    // if len == 0, return false, otherwise true.
    // this makes sure that if the string is empty we return false 
    // as it is not a digit, where as if the string is not empty
    // we know for sure that only digits are there.
    return i != 0;
}

// this function returns true if the string represents a floating or integer.
// numbers such as ".564" or "." or "3464." are accepted.
bool is_number(char * s) {
    char * t = s;
    int i;
    bool has_dot = false;
    for (i = 0; t[i] != '\0'; i++) {
        // allow dash for negative numbers on the first character.
        if (i == 0 && t[i] == '-')
            continue;
        // allow a dot as a number.
        if (!has_dot && t[i] == '.') {
            has_dot = true; // invert has_dot so we only accept one dot.
            continue;
        }
        // if t[i] is outside of 0 - 9 range character then return false
        if  (!isdigit(t[i])) {
            return false;
        }
    }
    return true;
}

// this function looks for the operation, if it doesn't find it
// it throws an error, however, if the user decides to not throw an error, it returns -1;
operation _find_operation(char * str_op, bool throw_error) {
    for (int i = 0; i<OPERATION_NUM; i++) {
        if (strcasecmp(str_op, str_operations[i]) == 0)
            return i;
    }
    // throw error if user wants to terminate program.
    if (throw_error) {
        fprintf(stderr, "FATAL ERROR in %s at line %d: operation `%s` not found.\n", __FILE__, __LINE__, str_op);
        exit(EXIT_FAILURE);
    }
    return -1;
}

// this function looks for the filter type, if it doesn't find it
// it throws an error, however, if the user decides to not throw an error, it returns -1;
filter_type _find_filter_type(char * str, bool throw_error) {
    for (int i = 0; i<FILTER_NUM; i++) {
        if (strcasecmp(str, str_filter_type[i]) == 0)
            return i;
    }
    // throw error if user wants to terminate program.
    if (throw_error) {
        fprintf(stderr, "FATAL ERROR in %s at line %d: filter_type not found.\n", __FILE__, __LINE__);
        exit(EXIT_FAILURE);
    }
    return -1;
}

bool is_EOL(char c) {
    return c == '\n' || c == '\r' || c == EOF;
}

bool is_separator(char c) {
    return  is_EOL(c) || c == ' ';
}
// this function accepts an arraylist of type char
// it loops and takes characters from the user input, and continues looping until 
// a full number is stored in the arraylist.
// the function returns the last character read.
char get_next_number(arraylist * a) {
    char terminal = '\0';
    char c = getchar();
    while(!is_separator(c)) {
        // add the character if condition is still true.
        add_element(a, &c);
        // continue looping until we reach EOF or c is \n, \r or space.
        // but c will not be added when we readh EOF, \n, \r or space since  the condition will be false.
        c = getchar();
    }
    // add a terminal character at the end.
    add_element(a, &terminal);
    // return the stopping character EOF, \n, \r or ' '
    return c;
}

// this function computes the operation, if the operation is an aggregate 
void compute_row(double * arr, int size, bool is_last_row) {
    operation op = get_int(&operation_param);
    // check if it is an aggregate function
    // since they are organized in the enum declaration, we can just check if the operation falls in the
    // aggregate section. Find the enum declaration at the top of this file.
    if (op >= COUNT && op <= PAVG) {
        double result = aggregate(str_operations[op], arr, size);
        // get format in memory allocation.
        char * format = get_prec_format();
        printf(format, result);
        free(format);
        // print new line if new_line is true. otherwise print space
        printf("%s", is_last_row? "\n":" ");
    }
    // Function is a singular function.
    else {
        // if Filter perform the filter operation and update the size of elements left.
        if (op == FILTER)
            size = filter(arr, size, f_type_param, threshold_param);
        else if (op == SHIFT)
            shift(arr, size, threshold_param);
        // print array after.
        print(arr, size);
    }    
}
// This function processes the next row in the stdin stream, it returns an arraylist of double.
arraylist * process_next_row() {
    // this array list is used for parsing each character individually.
    // by taking as input each character, they will be inserted in this arraylist
    // until EOF, or any other character is not an integer.
    arraylist * input_char_buffer = create_default_array(sizeof(char));

    // This arraylist serves as a buffer for all numbers stored.
    arraylist * input_double_buffer = create_default_array(sizeof(double));
    char last_char;
    // define the size if initialized, otherwise use max row.
    int size;
    if (size_param.is_init)
        size = size_param.val;
    else
        size = MAX_ROW_NUM;
    // loop for each element, and convert to double.
    do {
        last_char = get_next_number(input_char_buffer);
        // check if the buffer only has a \0;
        bool is_empty = strcmp(input_char_buffer->arr, "") == 0;
        // if buffer is not empty and the value it contains is not a number throw an error.
        if (!is_empty && !is_number(input_char_buffer->arr)) {
            // throw an error if the element parsed is not a number and not empty.
            fprintf(stderr, "FATAL ERROR in %s at line %d: invalid input %s.\n", __FILE__, __LINE__, (char *) input_char_buffer->arr);
            exit(EXIT_FAILURE);
        }
        else if(!is_empty) {
            double num = atof(input_char_buffer->arr);
            add_element(input_double_buffer, &num);
        }
        // remove all characters.
        input_char_buffer->len = 0;
    } while(!is_EOL(last_char) && input_double_buffer->len < size);
    
    // if the size param is not initialized we set it to the length of this array list.
    if (!size_param.is_init)
        set_int(&size_param, input_double_buffer->len);
    // free input char buffer
    free_arraylist(input_char_buffer);
    // reduce memory capacity of input_double_buffer -- not essential for the code to work.
    squish_elements(input_double_buffer);

    // compute_row(input_double_buffer->arr, input_double_buffer->len, is_last_row);
    return input_double_buffer;
}

void parse_and_initialize_arguments(int argc, char * argv[]){
    for (int i = 1; i < argc; i++) {
        // if substring of `-prec=` is found at beginning
        if (strcistr(argv[i], "-prec=") == argv[i]) {
            if (is_prec_init()) {
                fprintf(stderr, "FATAL ERROR in %s at line %d: repeated field `%s`.\n", __FILE__, __LINE__, argv[i]);
                exit(EXIT_FAILURE);
            }
            // find the starting character for the value of param=val
            char * ptr = strchr(argv[i], '=') + 1;
            // check if the value is an integer.
            if(!is_integer(ptr)) {
                fprintf(stderr, "FATAL ERROR in %s at line %d: invalid syntax for `%s`.\n", __FILE__, __LINE__, argv[i]);
                exit(EXIT_FAILURE);
            }
            // use set_prec from singular.c
            set_prec(atoi(ptr));

        }
        else if (strcistr(argv[i], "-size=") == argv[i]) {
            // throw error if size param is already initialized.
            if (size_param.is_init) {
                fprintf(stderr, "FATAL ERROR in %s at line %d: repeated field `%s`.\n", __FILE__, __LINE__, argv[i]);
                exit(EXIT_FAILURE);   
            }
            // find the next character after the '='
            char * ptr = strchr(argv[i], '=') + 1;
            // check if the value is an integer.
            if(!is_integer(ptr)) {
                fprintf(stderr, "FATAL ERROR in %s at line %d: invalid syntax for `%s`.\n", __FILE__, __LINE__, argv[i]);
                exit(EXIT_FAILURE);
            }
            set_int(&size_param, atoi(ptr));
        }
        else {
            // _find_operation will throw if there are no ops found.
            operation op =  _find_operation(argv[i], true);
            // check if the operation has already been defined in the past.
            if (operation_param.is_init) {
                fprintf(stderr, "FATAL ERROR in %s at line %d: cannot define more than one operation `%s`.\n", __FILE__, __LINE__, argv[i]);
                exit(EXIT_FAILURE);
            }
            // initialize global struct for the operation argument.
            set_int(&operation_param, op);

            if (op == FILTER) {
                // parse filter type
                i++;
                // if we reached out of bounds throw error.
                if(i >= argc) {
                    fprintf(stderr, "FATAL ERROR in %s at line %d: missing filter_type for operation `%s`.\n", __FILE__, __LINE__, argv[i-1]);
                    exit(EXIT_FAILURE);
                }
                f_type_param = _find_filter_type(argv[i], true);
            }
            if (op == SHIFT || op == FILTER) {
                // parse threshold in next field
                i++;
                if (i >= argc) {
                    fprintf(stderr, "FATAL ERROR in %s at line %d: missing threshold for operation `%s`.\n", __FILE__, __LINE__, str_operations[op]);
                    exit(EXIT_FAILURE);
                }
                if (!is_number(argv[i])) {
                    fprintf(stderr, "FATAL ERROR in %s at line %d: expected threshold number but found `%s` instead.\n", __FILE__, __LINE__, argv[i]);
                    exit(EXIT_FAILURE);
                }
                threshold_param = atof(argv[i]);
            }
        }
    }
}

int main(int argc, char * argv[]) {
    // parse arguments
    parse_and_initialize_arguments(argc, argv);

    // check if mandatory parameter operation is initialized.
    if (!operation_param.is_init) {
        fprintf(stderr, "FATAL ERROR in %s at line %d: missing required operation.\n", __FILE__, __LINE__);
        exit(EXIT_FAILURE);
    }

    // parse input from users.
    arraylist * rows = create_default_array(sizeof(arraylist));
    while(!feof(stdin)) {
        arraylist * single_row = process_next_row();
        add_element(rows, single_row);
        // we free the arraylist struct, but the pointer to the double array is still kept.
        // and used by the 
        free(single_row);
    }
    // check if the last row is empty, if so, we remove it
    arraylist * last_row = get_ptr_at_index(rows, rows->len - 1);
    if (last_row->len == 0) {
        free(last_row->arr);
        rows->len--;
    }

    for (int i = 0; i<rows->len; i++) {
        arraylist * row = get_ptr_at_index(rows, i);
        if (row->len != 0) {
            compute_row(row->arr, row->len, i == (rows->len-1));
        }
        // free the internal array of the row.
        free(row->arr);
    }
    free_arraylist(rows);
    return 0;
}